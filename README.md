# 商城

#### 介绍
仿天猫商城的一个javaweb项目

#### 软件架构
采用Servlet+jsp+javaBean的方式开发



#### 安装教程
数据库表在webcontent的sql目录下
将项目下载后导入eclipse即可使用；

#### 使用说明

1. 主页
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/153035_8ceb723e_2153770.png "miaoshu1.png")
2. 购物页
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/153257_9db0d96e_2153770.png "屏幕截图.png")
3. 购物车
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/153358_dc0a3dba_2153770.png "屏幕截图.png")
4. 后台管理
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/153419_49f00b35_2153770.png "屏幕截图.png")
5. 管理详情
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/153443_00966e3c_2153770.png "屏幕截图.png")
6. 新闻资讯页
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/153501_67a4d92c_2153770.png "屏幕截图.png")
7. 登录
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/153534_e85f1e02_2153770.png "屏幕截图.png")
8.注册
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)